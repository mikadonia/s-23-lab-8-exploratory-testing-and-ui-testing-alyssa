from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

options = webdriver.ChromeOptions()
options.binary_location = "/Applications/Google Chrome.app"
chrome_driver_binary = "/usr/local/bin/chromedriver"
driver = webdriver.Chrome(chrome_driver_binary, chrome_options=options)

driver.get("https://hd.kinopoisk.ru")
print(driver.title)
assert "Фильмы и сериалы смотреть онлайн в хорошем качестве по подписке — Кинопоиск" in driver.title

# LOGIN

login_button = driver.find_element_by_xpath(
    "/html/body/div[1]/div[2]/div[1]/header/div/div[3]/div/div[1]/a/span")
login_button.click()

try:
    mail_button = driver.find_element_by_xpath(
        "/html/body/div/div/div[2]/div[2]/div/div/div[2]/div[3]/div/div/div/div[1]/form/div[1]/div[2]/button")
    mail_button.click()

    login_field = driver.find_element(
        By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div/div[2]/div[3]/div/div/div/div[1]/form/div[2]/div/span/input')
    login_field.send_keys("YOUR USERNAME GOES HERE")

    login_button_1 = driver.find_element(
        By.XPATH, '/html/body/div/div/div[2]/div[2]/div/div/div[2]/div[3]/div/div/div/div[1]/form/div[4]/button')
    login_button_1.click()

except:
    login_field = driver.find_element_by_xpath(
        "/html/body/div/div/div[2]/div[2]/div/div/div[2]/div[3]/div/div/div/div[1]/form/div[1]/span/input")
    login_field.send_keys("YOUR USERNAME GOES HERE")

    login_button_1 = driver.find_element_by_xpath(
        "/html/body/div/div/div[2]/div[2]/div/div/div[2]/div[3]/div/div/div/div[1]/form/div[3]/button")
    login_button_1.click()

time.sleep(1)

password_field = driver.find_element(
    By.XPATH, "/html/body/div/div/div[2]/div[2]/div/div/div[2]/div[3]/div/div/div/form/div[2]/span/input")
password_field.send_keys("YOUR PASSWORD GOES HERE")

login_button_2 = driver.find_element_by_id("passp:sign-in")
login_button_2.click()

time.sleep(1)

if 'Выберите профиль' in driver.title:
    temp = driver.find_element(
        By.XPATH, '/html/body/div[1]/div[2]/div/div/div/div/div/a')
    temp.click()

# FIND FILM
time.sleep(1)

find_button = driver.find_element(
    By.XPATH, '/html/body/div[1]/div[2]/div[1]/header/div/div[2]/div/div[1]/div/button')
find_button.click()

find_field = driver.find_element(
    By.XPATH, '/html/body/div[1]/div[2]/div[1]/header/div/div[2]/div/div[2]/div/div[1]/input')
find_field.send_keys("Побег")

time.sleep(1)

found_element = driver.find_element(
    By.XPATH, '/html/body/div[1]/div[2]/div[1]/header/div/div[2]/div/div[2]/div/div[2]/div[2]/div/div/div/div[2]/div[1]/a')

target_elem_link = found_element.get_attribute('href')

found_element.click()

time.sleep(1)

watch_later_button = driver.find_element(
    By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div/div/div[1]/div/div/section/div/div[2]/div/div/section/div[2]/div/div/div/div[3]/div/button')
watch_later_button.click()

time.sleep(1)
driver.get('https://hd.kinopoisk.ru/watchlist')

time.sleep(1)

last_added = driver.find_element(
    By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div/div/div/div/div/div[2]/div[1]/div[1]/div/div[1]/div/a')


assert last_added.get_property(
    'href') == target_elem_link, "Found element should be in Watch later list"

time.sleep(10)
driver.close()
